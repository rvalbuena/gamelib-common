package com.ramonx86.common.game.input.gamepad;


import com.ramonx86.common.game.input.gamepad.mapping.*;
import com.ramonx86.common.utility.OS;

/**
 * Check the identified controller name and instantiate a matching
 * GamePad instance
 * This needs to be updated whenever there is a new mapping
 * Created by ramon on 11/19/15.
 */
public class GamePadIdentifier {

    public enum KNOWN_GAMEPADS {
        DEFAULT_XBOX,
        OUYA,
        GS_GAMEPAD,
        SNES,
        MOGA,
        GREEN_THROTTLE_ATLAS,
        LOGITECH,
        NYKO_PLAYPAD_PRO,
        GAMESTICK_CONTROLLER,
        NVIDIA_SHIELD,
        AMAZON_FIRE_TV,
        RAZER_SERVAL
    };

    public static GamePad identify(final String controllerId, final boolean useDefault) {
        GamePad result = null;
        String cid = controllerId.toUpperCase();
        System.out.println(OS.platform);
        System.out.println(cid);
        if (cid.contains("XBOX") || cid.contains("X-BOX")) {
            result = new GamePad(cid);
        }
        else
        if (cid.contains("OUYA")) {
            result = new Ouya(cid);
        }
        else
        if (cid.contains("GS GAMEPAD")) {
            result = new Gamestop(cid);
        }
        else
        if (cid.contains("SNES")) {
            result = new SNES(cid);
        }
        else
        if (cid.contains("MOGA")) {
            result = new Moga(cid);
        }
        else
        if (cid.contains("GREEN THROTTLE ATLAS")) {
            result = new GreenThrottle(cid);
        }
        else
        if (cid.contains("LOGITECH") && !cid.contains("UNIFYING DEVICE")) {
            // some Razer Forge component identifies as Logitech Unifying Device
            result = new Logitech(cid);
        }
        else
        if (cid.contains("NYKO PLAYPAD PRO")) {
            result = new NykoPlaypad(cid);
        }
        else
        if (cid.contains("GAMESTICK CONTROLLER")) {
            result = new GameStick(cid);
        }
        else
        if (cid.contains("NVIDIA")) {
            //
            result = new NvidiaShield(cid);
        }
        else
        if (cid.contains("AMAZON FIRE GAME CONTROLLER")) {
            result = new AmazonFireTV(cid);
        }
        else
        if (cid.contains("ASUS GAMEPAD") || cid.contains("RAZER SERVAL")) {
            // the Serval's ID was "ASUS Gamepad" on the Razer Forge and "Razer Serval on Linux
            result = new RazerServal(cid);
        }
        else {
            if (useDefault) {
                //TODO: log that we used the default
                result = new GamePad(cid);// default/Xbox compatible
            }
        }
        return result;
    }

    public static GamePad identify(final String controllerId) {
        return identify(controllerId,false);
    }
}
