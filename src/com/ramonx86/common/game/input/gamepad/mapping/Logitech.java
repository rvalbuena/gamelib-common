package com.ramonx86.common.game.input.gamepad.mapping;

import com.ramonx86.common.utility.OS;

/**
 * Logitech Dual Action F310?
 * May also work for other Logitech dual sticks,
 * "Dual action" has a button that swaps the dpad and left analog
 *
 * Created by ramon on 11/19/15.
 */
public class Logitech extends GamePad {

    public Logitech(String modelName){
        super(modelName);
        if (OS.platform.equals(OS.Platform.Windows)) {
            /* Detected as Logitech Dual Action */
            BUTTON_A = 1;
            BUTTON_X = 0;
            BUTTON_Y = 3;
            BUTTON_B = 2;
            BUTTON_MENU = NOT_USED;
            DPAD_IS_POV = true;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = false;
            AXIS_DPAD_X = NOT_USED;
            AXIS_DPAD_Y = NOT_USED;
            BUTTON_L1 /* bumper */= 4;
            BUTTON_L2 /* trigger */= 6;
            BUTTON_L3 /* joystick */= 10;
            BUTTON_R1 /* bumper */= 5;
            BUTTON_R2 /* trigger */= 7;
            BUTTON_R3 /* joystick */= 11;
            AXIS_LEFT_X = 3;
            AXIS_LEFT_Y = 2;
            AXIS_LEFT_TRIGGER = NOT_USED;
            AXIS_RIGHT_X = 0;
            AXIS_RIGHT_Y = 1;
            AXIS_RIGHT_TRIGGER = NOT_USED;
            BUTTON_BACK = 8;
            BUTTON_START = 9;
            return;
		}
        if (OS.platform.equals(OS.Platform.Linux)) {
            /* Logitech Logitech Dual Action */
            BUTTON_A = 1;
            BUTTON_X = 0;
            BUTTON_Y = 3;
            BUTTON_B = 2;
            BUTTON_MENU = 8;
            DPAD_IS_POV = true;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = false;
            AXIS_DPAD_X = NOT_USED;
            AXIS_DPAD_Y = NOT_USED;
            BUTTON_L1 /* bumper */= 4;
            BUTTON_L2 /* trigger */= 6;
            BUTTON_L3 /* joystick */= 10;
            BUTTON_R1 /* bumper */= 5;
            BUTTON_R2 /* trigger */= 7;
            BUTTON_R3 /* joystick */= 11;
            AXIS_LEFT_X = 0;
            AXIS_LEFT_Y = 1;
            AXIS_LEFT_TRIGGER = NOT_USED;
            AXIS_RIGHT_X = 2;
            AXIS_RIGHT_Y = 3;
            AXIS_RIGHT_TRIGGER = NOT_USED;
            BUTTON_BACK = 8;
            BUTTON_START = 9;
            return;
        }
        if (OS.platform.equals(OS.Platform.Android)) {
            /* detected as Logitech Logitech Dual Action */
            BUTTON_A = 99;// "2" A position
            BUTTON_X = 96;// "1" X post
            BUTTON_Y = 100;// "4" Y position
            BUTTON_B = 97; // "3" B position
            BUTTON_MENU = NOT_USED;
            DPAD_IS_POV = false;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = true;
            AXIS_DPAD_X = 4;
            AXIS_DPAD_Y = 5;
            BUTTON_L1 /* bumper */= 102;
            BUTTON_L2 /* trigger */= 104;
            BUTTON_L3 /* joystick */= 106;
            BUTTON_R1 /* bumper */= 103;
            BUTTON_R2 /* trigger */= 105;
            BUTTON_R3 /* joystick */= 107;
            AXIS_LEFT_X = 1;
            AXIS_LEFT_Y = 0;
            AXIS_LEFT_TRIGGER = NOT_USED;
            AXIS_RIGHT_X = 2;
            AXIS_RIGHT_Y = 3;
            AXIS_RIGHT_TRIGGER = NOT_USED;
            BUTTON_BACK = 4;
            BUTTON_START = 108;
            return;
        }
    }
}
