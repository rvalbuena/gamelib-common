package com.ramonx86.common.game.input.gamepad.mapping;

import com.ramonx86.common.utility.OS;

/**
 * Created by ramon on 11/18/15.
 */
public class SNES extends GamePad {

    public SNES(String modelName){
        super(modelName);
        if (OS.platform.equals(OS.Platform.Linux)) {
            /* by AngelusWeb » Fri Sep 06, 2013 11:33 pm */
            BUTTON_A = 0;
            BUTTON_X = 2;
            BUTTON_Y = 3;
            BUTTON_B = 1;
            BUTTON_MENU = NOT_USED;
            DPAD_IS_POV = false;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = true;
            AXIS_DPAD_X = 0;
            AXIS_DPAD_Y = 1;
            BUTTON_L1 /* bumper */= 5;
            BUTTON_L2 /* trigger */= NOT_USED;
            BUTTON_L3 /* joystick */= NOT_USED;
            BUTTON_R1 /* bumper */= 6;
            BUTTON_R2 /* trigger */= NOT_USED;
            BUTTON_R3 /* joystick */= NOT_USED;
            AXIS_LEFT_X = NOT_USED;
            AXIS_LEFT_Y = NOT_USED;
            AXIS_LEFT_TRIGGER = NOT_USED;
            AXIS_RIGHT_X = NOT_USED;
            AXIS_RIGHT_Y = NOT_USED;
            AXIS_RIGHT_TRIGGER = NOT_USED;
            BUTTON_BACK = NOT_USED;
            BUTTON_START = NOT_USED;
            return;
        }
        /* default SNES mapping */
        BUTTON_A = 0;
        BUTTON_X = 2;
        BUTTON_Y = 3;
        BUTTON_B = 1;
        BUTTON_MENU = NOT_USED;
        DPAD_IS_POV = false;
        DPAD_IS_BUTTON = false;
        BUTTON_DPAD_UP = NOT_USED;
        BUTTON_DPAD_DOWN = NOT_USED;
        BUTTON_DPAD_RIGHT = NOT_USED;
        BUTTON_DPAD_LEFT = NOT_USED;
        DPAD_IS_AXIS = true;
        AXIS_DPAD_X = 0;
        AXIS_DPAD_Y = 1;
        BUTTON_L1 /* bumper */= 5;
        BUTTON_L2 /* trigger */= NOT_USED;
        BUTTON_L3 /* joystick */= NOT_USED;
        BUTTON_R1 /* bumper */= 6;
        BUTTON_R2 /* trigger */= NOT_USED;
        BUTTON_R3 /* joystick */= NOT_USED;
        AXIS_LEFT_X = NOT_USED;
        AXIS_LEFT_Y = NOT_USED;
        AXIS_LEFT_TRIGGER = NOT_USED;
        AXIS_RIGHT_X = NOT_USED;
        AXIS_RIGHT_Y = NOT_USED;
        AXIS_RIGHT_TRIGGER = NOT_USED;
        BUTTON_BACK = NOT_USED;
        BUTTON_START = NOT_USED;
    }
}
