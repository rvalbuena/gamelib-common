package com.ramonx86.common.game.input.gamepad.mapping;

import com.ramonx86.common.utility.OS;

/**
 * NVIDIA Shield TV controller
 * Created by ramon on 8/30/16.
 *
 * Mappings may be the same for Shield Portable
 * Supposedly this controller can be used via USB on PC with an NVIDIA/GEFORCE utility
 * Wireless is wifi-direct and not bluetooth so it does not work with other devices
 */
public class NvidiaShield extends GamePad {

    public NvidiaShield(String modelName) {
        super(modelName);
        if (OS.platform.equals(OS.Platform.Android)) {
            // checked only on NVidia Shield TV
            BUTTON_A = 96;
            BUTTON_X = 99;
            BUTTON_Y = 100;
            BUTTON_B = 97;
            BUTTON_MENU = NOT_USED;
            DPAD_IS_POV = false;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = true;
            AXIS_DPAD_X = 8;
            AXIS_DPAD_Y = 9;
            BUTTON_L1 /* bumper */= 102;
            BUTTON_L2 /* trigger */= NOT_USED;
            BUTTON_L3 /* joystick */= 106;
            BUTTON_R1 /* bumper */= 103;
            BUTTON_R2 /* trigger */= NOT_USED;
            BUTTON_R3 /* joystick */= 107;
            AXIS_LEFT_X = 0;
            AXIS_LEFT_Y = 1;
            AXIS_LEFT_TRIGGER = 6; //actually 6 & 7
            AXIS_RIGHT_X = 2;
            AXIS_RIGHT_Y = 3;
            AXIS_RIGHT_TRIGGER = 4; // actually 4 & 5
            BUTTON_BACK = 4;
            BUTTON_START = 108;
            return;
        }

    }
}
