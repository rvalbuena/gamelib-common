package com.ramonx86.common.game.input.gamepad.mapping;

import com.ramonx86.common.utility.OS;

/**
 * Created by ramon on 11/18/15.
 */
public class GreenThrottle extends GamePad {

    public GreenThrottle(String modelName){
        super(modelName);
        if (OS.platform.equals(OS.Platform.Linux)) {
            /* Detected as "Green Throttle Atlas" */
            BUTTON_A = 11;
            BUTTON_X = 14;
            BUTTON_Y = 15;
            BUTTON_B = 12;
            BUTTON_MENU = NOT_USED;//
            DPAD_IS_POV = true;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = false;
            AXIS_DPAD_X = NOT_USED;
            AXIS_DPAD_Y = NOT_USED;
            BUTTON_L1 /* bumper */= 17;
            BUTTON_L2 /* trigger */= NOT_USED;
            BUTTON_L3 /* joystick */= 24;
            BUTTON_R1 /* bumper */= 18;
            BUTTON_R2 /* trigger */= NOT_USED;
            BUTTON_R3 /* joystick */= 25;
            AXIS_LEFT_X = 0;
            AXIS_LEFT_Y = 1;
            AXIS_LEFT_TRIGGER = 5;
            AXIS_RIGHT_X = 2;
            AXIS_RIGHT_Y = 3;
            AXIS_RIGHT_TRIGGER = 4;
            BUTTON_BACK = 2;
            BUTTON_START = 22;
            return;
        }
        if (OS.platform.equals(OS.Platform.Windows)) {
			// NOTE: does not appear to be able to connect properly to Windows
            // TODO: not checked yet
            BUTTON_A = 0;
            BUTTON_X = 2;
            BUTTON_Y = 3;
            BUTTON_B = 1;
            BUTTON_MENU = NOT_USED;
            DPAD_IS_POV = true;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = false;
            AXIS_DPAD_X = NOT_USED;
            AXIS_DPAD_Y = NOT_USED;
            BUTTON_L1 /* bumper */= 4;
            BUTTON_L2 /* trigger */= 6;
            BUTTON_L3 /* joystick */= 10;
            BUTTON_R1 /* bumper */= 5;
            BUTTON_R2 /* trigger */= 7;
            BUTTON_R3 /* joystick */= 11;
            AXIS_LEFT_X = 3;
            AXIS_LEFT_Y = 2;
            AXIS_LEFT_TRIGGER = NOT_USED;
            AXIS_RIGHT_X = 3;
            AXIS_RIGHT_Y = 2;
            AXIS_RIGHT_TRIGGER = NOT_USED;
            BUTTON_BACK = 8;
            BUTTON_START = 9;
            return;
        }
        if (OS.platform.equals(OS.Platform.Android)) {
            // checked on Kindle Fire HD6
            BUTTON_A = 96;
            BUTTON_X = 99;
            BUTTON_Y = 100;
            BUTTON_B = 97;
            BUTTON_MENU = NOT_USED;
            DPAD_IS_POV = false;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = true;
            AXIS_DPAD_X = 6;
            AXIS_DPAD_Y = 7;
            BUTTON_L1 /* bumper */= 102;
            BUTTON_L2 /* trigger */= NOT_USED;
            BUTTON_L3 /* joystick */= 106;
            BUTTON_R1 /* bumper */= 103;
            BUTTON_R2 /* trigger */= NOT_USED;
            BUTTON_R3 /* joystick */= 107;
            AXIS_LEFT_X = 0;
            AXIS_LEFT_Y = 1;
            AXIS_LEFT_TRIGGER = 5;
            AXIS_RIGHT_X = 2;
            AXIS_RIGHT_Y = 3;
            AXIS_RIGHT_TRIGGER = 4;
            BUTTON_BACK = 4;
            BUTTON_START = 108;
            return;
        }
    }
}
