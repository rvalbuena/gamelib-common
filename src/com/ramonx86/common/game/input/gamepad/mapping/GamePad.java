package com.ramonx86.common.game.input.gamepad.mapping;

import com.ramonx86.common.utility.OS;

/**
 * Based on GamePadMap posted on libgdx/Ouya forums
 * Separating each identified controller into it's own class for easier maintenance
 * Made properties read-only, everything must be set in constructor
 * DONE: Renamed buttons OUYA back to ABXY
 * TODO: add one to represent user-customized button mappings
 * Created by ramon on 11/18/15.
 */
public class GamePad {

    public static int NOT_USED = -1;

    protected int AXIS_DPAD_X;
    protected int AXIS_DPAD_Y;
    protected int AXIS_LEFT_TRIGGER;
    protected int AXIS_LEFT_X;
    protected int AXIS_LEFT_Y;
    protected int AXIS_RIGHT_TRIGGER;
    protected int AXIS_RIGHT_X;
    protected int AXIS_RIGHT_Y;
    protected int BUTTON_A;
    protected int BUTTON_B;
    protected int BUTTON_BACK;
    protected int BUTTON_DPAD_DOWN;
    protected int BUTTON_DPAD_LEFT;
    protected int BUTTON_DPAD_RIGHT;
    protected int BUTTON_DPAD_UP;
    protected int BUTTON_L1 /* bumper */;
    protected int BUTTON_L2 /* trigger */;
    protected int BUTTON_L3 /* joystick */;
    protected int BUTTON_MENU;
    protected int BUTTON_R1 /* bumper */;
    protected int BUTTON_R2 /* trigger */;
    protected int BUTTON_R3 /* joystick */;
    protected int BUTTON_START;
    protected int BUTTON_X;
    protected int BUTTON_Y;
    protected boolean DPAD_IS_AXIS;
    protected boolean DPAD_IS_BUTTON;
    protected boolean DPAD_IS_POV;

    //public final Model model;
    protected String model;
    
    protected int[] codes;

    public GamePad(String modelName){
        model = modelName; // get the actual identified name for debugging
        if (OS.platform.equals(OS.Platform.Linux)) {
            /* "Generic X-Box pad" */
            BUTTON_A = 0;
            BUTTON_B = 1;
            BUTTON_X = 2;
            BUTTON_Y = 3;
            BUTTON_MENU = 8;
            DPAD_IS_POV = true;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;;
            BUTTON_DPAD_DOWN = NOT_USED;;
            BUTTON_DPAD_RIGHT = NOT_USED;;
            BUTTON_DPAD_LEFT = NOT_USED;;
            DPAD_IS_AXIS = false;
            AXIS_DPAD_X = NOT_USED;;
            AXIS_DPAD_Y = NOT_USED;;
            BUTTON_L1 /* bumper */= 4;
            BUTTON_L2 /* trigger */= NOT_USED;;
            BUTTON_L3 /* joystick */= 9;
            BUTTON_R1 /* bumper */= 5;
            BUTTON_R2 /* trigger */= NOT_USED;;
            BUTTON_R3 /* joystick */= 10;
            AXIS_LEFT_X = 0;
            AXIS_LEFT_Y = 1;
            AXIS_LEFT_TRIGGER = 2;
            AXIS_RIGHT_X = 3;
            AXIS_RIGHT_Y = 4;
            AXIS_RIGHT_TRIGGER = 5;
            BUTTON_BACK = 6;
            BUTTON_START = 7;
            return;
        }
        if (OS.platform.equals(OS.Platform.Windows)) {
            /* Rock Candy Gamepad for Xbox 360 */
            BUTTON_A = 0;
            BUTTON_B = 1;
            BUTTON_X = 2;
            BUTTON_Y = 3;
            BUTTON_MENU = NOT_USED;// tries to open "game bar"
            DPAD_IS_POV = true;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;;
            BUTTON_DPAD_DOWN = NOT_USED;;
            BUTTON_DPAD_RIGHT = NOT_USED;;
            BUTTON_DPAD_LEFT = NOT_USED;;
            DPAD_IS_AXIS = false;
            AXIS_DPAD_X = NOT_USED;;
            AXIS_DPAD_Y = NOT_USED;;
            BUTTON_L1 /* bumper */= 4;
            BUTTON_L2 /* trigger */= NOT_USED;;
            BUTTON_L3 /* joystick */= 3;
            BUTTON_R1 /* bumper */= 5;
            BUTTON_R2 /* trigger */= NOT_USED;;
            BUTTON_R3 /* joystick */= 9;
            AXIS_LEFT_X = 1;
            AXIS_LEFT_Y = 0;
            AXIS_LEFT_TRIGGER = 4;// positive values
            AXIS_RIGHT_X = 3;
            AXIS_RIGHT_Y = 2;
            AXIS_RIGHT_TRIGGER = 4;// negative values // This is for real, same axis # as
            // for LEFT trigger!
            BUTTON_BACK = 6;
            BUTTON_START = 7;
            return;
        }
        if (OS.platform.equals(OS.Platform.Android)) {
            /* on NVIDIA shield detected as "Generic X-Box pad" */
            BUTTON_A = 96;
            BUTTON_X = 99;
            BUTTON_Y = 100;
            BUTTON_B = 97;
            BUTTON_MENU = 108;//? not catching menu key
            DPAD_IS_POV = false;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;// axis(7)
            BUTTON_DPAD_DOWN = NOT_USED;// axis(7)
            BUTTON_DPAD_RIGHT = NOT_USED;// axis(6)
            BUTTON_DPAD_LEFT = NOT_USED;// axis(6)
            DPAD_IS_AXIS = true;
            AXIS_DPAD_X = 8;
            AXIS_DPAD_Y = 9;
            BUTTON_L1 /* bumper */= 102;
            BUTTON_L2 /* trigger */= NOT_USED; // axis(2)
            BUTTON_L3 /* joystick */= 106;
            BUTTON_R1 /* bumper */= 103;
            BUTTON_R2 /* trigger */= NOT_USED; // axis(5)
            BUTTON_R3 /* joystick */= 107;
            AXIS_LEFT_X = 0;
            AXIS_LEFT_Y = 1;
            AXIS_LEFT_TRIGGER = 2;//actually 2 & 3
            AXIS_RIGHT_X = 4;
            AXIS_RIGHT_Y = 5;
            AXIS_RIGHT_TRIGGER = 6; // actually 6 & 7
            BUTTON_BACK = 4;
            BUTTON_START = 108;
            return;
        }
    }

    public int getAxisDpadX() {
        return AXIS_DPAD_X;
    }

    public int getAxisDpadY() {
        return AXIS_DPAD_Y;
    }

    public int getAxisLeftTrigger() {
        return AXIS_LEFT_TRIGGER;
    }

    public int getAxisLeftX() {
        return AXIS_LEFT_X;
    }

    public int getAxisLeftY() {
        return AXIS_LEFT_Y;
    }

    public int getAxisRightTrigger() {
        return AXIS_RIGHT_TRIGGER;
    }

    public int getAxisRightX() {
        return AXIS_RIGHT_X;
    }

    public int getAxisRightY() {
        return AXIS_RIGHT_Y;
    }

    public int getButtonB() {
        return BUTTON_B;
    }

    public int getButtonBack() {
        return BUTTON_BACK;
    }

    public int getButtonDpadDown() {
        return BUTTON_DPAD_DOWN;
    }

    public int getButtonDpadLeft() {
        return BUTTON_DPAD_LEFT;
    }

    public int getButtonDpadRight() {
        return BUTTON_DPAD_RIGHT;
    }

    public int getButtonDpadUp() {
        return BUTTON_DPAD_UP;
    }

    public int getButtonL1() {
        return BUTTON_L1;
    }

    public int getButtonL2() {
        return BUTTON_L2;
    }

    public int getButtonL3() {
        return BUTTON_L3;
    }

    public int getButtonMenu() {
        return BUTTON_MENU;
    }

    public int getButtonA() {
        return BUTTON_A;
    }

    public int getButtonR1() {
        return BUTTON_R1;
    }

    public int getButtonR2() {
        return BUTTON_R2;
    }

    public int getButtonR3() {
        return BUTTON_R3;
    }

    public int getButtonStart() {
        return BUTTON_START;
    }

    public int getButtonX() {
        return BUTTON_X;
    }

    public int getButtonY() {
        return BUTTON_Y;
    }

    public boolean isDpadAxis() {
        return DPAD_IS_AXIS;
    }

    public boolean isDpadButton() {
        return DPAD_IS_BUTTON;
    }

    public boolean isDpadPOV() {
        return DPAD_IS_POV;
    }

    public String getModel() {
        return model;
    }

}
