package com.ramonx86.common.game.input.gamepad.mapping;

import com.ramonx86.common.utility.OS;

/**
 * Amazon Fire TV Game Controller
 * Created by ramon on 9/1/16.
 */
public class AmazonFireTV extends GamePad {

    public AmazonFireTV(String modelName) {
        super(modelName);
        if (OS.platform.equals(OS.Platform.Android)) {
            // checked only natively on Fire TV
            BUTTON_A = 96;
            BUTTON_X = 99;
            BUTTON_Y = 100;
            BUTTON_B = 97;
            BUTTON_MENU = 82;
            DPAD_IS_POV = false;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = true;
            AXIS_DPAD_X = 6;
            AXIS_DPAD_Y = 7;
            BUTTON_L1 /* bumper */= 102;
            BUTTON_L2 /* trigger */= NOT_USED;
            BUTTON_L3 /* joystick */= 106;
            BUTTON_R1 /* bumper */= 103;
            BUTTON_R2 /* trigger */= NOT_USED;
            BUTTON_R3 /* joystick */= 107;
            AXIS_LEFT_X = 0;
            AXIS_LEFT_Y = 1;
            AXIS_LEFT_TRIGGER = 5;
            AXIS_RIGHT_X = 2;
            AXIS_RIGHT_Y = 3;
            AXIS_RIGHT_TRIGGER = 4;
            BUTTON_BACK = 4;
            BUTTON_START = 108;
            return;
        }
    }
}
