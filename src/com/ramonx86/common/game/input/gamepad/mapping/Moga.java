package com.ramonx86.common.game.input.gamepad.mapping;

import com.ramonx86.common.utility.OS;

/**
 * Moga Pro controller in HID mode
 * Created by ramon on 11/18/15.
 */
public class Moga extends GamePad {

    public Moga(String modelName){
        super(modelName);
        if (OS.platform.equals(OS.Platform.Windows)) {
            /*
            * Detected as "Android Controller Gen-2(ACC)" by libgdx 1.9.4 controllers
            * Windows bluetooth settings shows "Moga 2 HID"
            * */
            BUTTON_A = 0;
            BUTTON_X = 2;
            BUTTON_Y = 3;
            BUTTON_B = 1;
            BUTTON_MENU = NOT_USED;
            DPAD_IS_POV = true;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = false;
            AXIS_DPAD_X = NOT_USED;
            AXIS_DPAD_Y = NOT_USED;
            BUTTON_L1 /* bumper */= 4;
            BUTTON_L2 /* trigger */= NOT_USED;
            BUTTON_L3 /* joystick */= 8;
            BUTTON_R1 /* bumper */= 5;
            BUTTON_R2 /* trigger */= NOT_USED;
            BUTTON_R3 /* joystick */= 9;
            AXIS_LEFT_X = 3;
            AXIS_LEFT_Y = 2;
            AXIS_LEFT_TRIGGER = NOT_USED; // no events
            AXIS_RIGHT_X = 1;
            AXIS_RIGHT_Y = 0;
            AXIS_RIGHT_TRIGGER = NOT_USED;// no events
            BUTTON_BACK = 6; // select
            BUTTON_START = 7;
            return;
        }
        if (OS.platform.equals(OS.Platform.Linux)) {
            /* Moga 2 HID */
            BUTTON_A = 1;
            BUTTON_X = 3;
            BUTTON_Y = 4;
            BUTTON_B = 2;
            BUTTON_MENU = NOT_USED;
            DPAD_IS_POV = true;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = false;
            AXIS_DPAD_X = NOT_USED;
            AXIS_DPAD_Y = NOT_USED;
            BUTTON_L1 /* bumper */= 5;
            BUTTON_L2 /* trigger */= NOT_USED;
            BUTTON_L3 /* joystick */= 8;
            BUTTON_R1 /* bumper */= 6;
            BUTTON_R2 /* trigger */= NOT_USED;
            BUTTON_R3 /* joystick */= 9;
            AXIS_LEFT_X = 0;
            AXIS_LEFT_Y = 1;
            AXIS_LEFT_TRIGGER = 5;
            AXIS_RIGHT_X = 2;
            AXIS_RIGHT_Y = 3;
            AXIS_RIGHT_TRIGGER = 4;
            BUTTON_BACK = 0;
            BUTTON_START = 7;
            return;
        }
        if (OS.platform.equals(OS.Platform.Android)) {
            /*  detected as Moga 2 HID */
            BUTTON_A = 96;
            BUTTON_X = 99;
            BUTTON_Y = 100;
            BUTTON_B = 97;
            BUTTON_MENU = NOT_USED;
            DPAD_IS_POV = false;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = true;
            AXIS_DPAD_X = 6;
            AXIS_DPAD_Y = 7;
            BUTTON_L1 /* bumper */= 102;
            BUTTON_L2 /* trigger */= NOT_USED;
            BUTTON_L3 /* joystick */= 106;
            BUTTON_R1 /* bumper */= 103;
            BUTTON_R2 /* trigger */= NOT_USED;
            BUTTON_R3 /* joystick */= 107;
            AXIS_LEFT_X = 0;
            AXIS_LEFT_Y = 1;
            AXIS_LEFT_TRIGGER = 5;
            AXIS_RIGHT_X = 2;
            AXIS_RIGHT_Y = 3;
            AXIS_RIGHT_TRIGGER = 4;
            BUTTON_BACK = 4;
            BUTTON_START = 108;
            return;
        }
    }
}
