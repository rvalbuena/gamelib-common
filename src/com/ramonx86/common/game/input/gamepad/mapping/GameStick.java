package com.ramonx86.common.game.input.gamepad.mapping;

import com.ramonx86.common.utility.OS;

/**
 * GameStick controller
 * Created by ramon on 11/18/15.
 */
public class GameStick extends GamePad {

    public GameStick(String modelName){
        super(modelName);
        if (OS.platform.equals(OS.Platform.Linux)) {
            /* detected as GameStick Controller */
            BUTTON_A = 170;
            BUTTON_X = 173;
            BUTTON_Y = 174;
            BUTTON_B = 171;
            BUTTON_MENU = 144;
            DPAD_IS_POV = true;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = false;
            AXIS_DPAD_X = NOT_USED;
            AXIS_DPAD_Y = NOT_USED;
            BUTTON_L1 /* bumper */= 176;
            BUTTON_L2 /* trigger */= NOT_USED;
            BUTTON_L3 /* joystick */= 183;
            BUTTON_R1 /* bumper */= 177;
            BUTTON_R2 /* trigger */= 16;
            BUTTON_R3 /* joystick */= 184;
            AXIS_LEFT_X = 0;
            AXIS_LEFT_Y = 1;
            AXIS_LEFT_TRIGGER = NOT_USED;
            AXIS_RIGHT_X = 2;
            AXIS_RIGHT_Y = 3;
            AXIS_RIGHT_TRIGGER = NOT_USED;
            BUTTON_BACK = 137;
            BUTTON_START = 181;
            return;
        }
        if (OS.platform.equals(OS.Platform.Windows)) {
            /* libgdx 1.9.4 detected as "Bluetooth v3.0 HID"
             * Windows bluetooth settings detected as "GameStick Controller" */
            BUTTON_A = 0;
            BUTTON_X = 3;
            BUTTON_Y = 4;
            BUTTON_B = 1;
            BUTTON_MENU = NOT_USED;// launches browser
            DPAD_IS_POV = true;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = false;
            AXIS_DPAD_X = NOT_USED;
            AXIS_DPAD_Y = NOT_USED;
            BUTTON_L1 /* bumper */= 6;
            BUTTON_L2 /* trigger */= NOT_USED;
            BUTTON_L3 /* joystick */= 13;
            BUTTON_R1 /* bumper */= 7;
            BUTTON_R2 /* trigger */= NOT_USED;
            BUTTON_R3 /* joystick */= 14;
            AXIS_LEFT_X = 3;
            AXIS_LEFT_Y = 2;
            AXIS_LEFT_TRIGGER = NOT_USED;
            AXIS_RIGHT_X = 1;
            AXIS_RIGHT_Y = 0;
            AXIS_RIGHT_TRIGGER = NOT_USED;
            BUTTON_BACK = NOT_USED;//special key event
            BUTTON_START = 11;
            return;
        }
        if (OS.platform.equals(OS.Platform.Android)) {
            /*  Detected as GameStick Controller */
            BUTTON_A = 96;
            BUTTON_X = 99;
            BUTTON_Y = 100;
            BUTTON_B = 97;
            BUTTON_MENU = 109;
            DPAD_IS_POV = false;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = true;
            AXIS_DPAD_X = 6;
            AXIS_DPAD_Y = 7;
            BUTTON_L1 /* bumper */= 102;
            BUTTON_L2 /* trigger */= NOT_USED;
            BUTTON_L3 /* joystick */= 106;
            BUTTON_R1 /* bumper */= 103;
            BUTTON_R2 /* trigger */= NOT_USED;
            BUTTON_R3 /* joystick */= 107;
            AXIS_LEFT_X = 1;
            AXIS_LEFT_Y = 0;
            AXIS_LEFT_TRIGGER = NOT_USED;
            AXIS_RIGHT_X = 2;
            AXIS_RIGHT_Y = 3;
            AXIS_RIGHT_TRIGGER = NOT_USED;
            BUTTON_BACK = 109;
            BUTTON_START = 108;
            return;
        }
    }

}
