package com.ramonx86.common.game.input.gamepad.mapping;

import com.ramonx86.common.utility.OS;

/**
 * Created by ramon on 9/1/16.
 */
public class RazerServal extends GamePad {

    public RazerServal(String modelName) {
        super(modelName);
        if (OS.platform.equals(OS.Platform.Linux)) {
            /* detected as Razer Serval */
            BUTTON_A = 2;
            BUTTON_X = 4;
            BUTTON_Y = 5;
            BUTTON_B = 3;
            BUTTON_MENU = 1;
            DPAD_IS_POV = true;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = false;
            AXIS_DPAD_X = NOT_USED;
            AXIS_DPAD_Y = NOT_USED;
            BUTTON_L1 /* bumper */= 6;
            BUTTON_L2 /* trigger */= NOT_USED;
            BUTTON_L3 /* joystick */= 11;
            BUTTON_R1 /* bumper */= 7;
            BUTTON_R2 /* trigger */= NOT_USED;
            BUTTON_R3 /* joystick */= 12;
            AXIS_LEFT_X = 0;
            AXIS_LEFT_Y = 1;
            AXIS_LEFT_TRIGGER = 5;
            AXIS_RIGHT_X = 2;
            AXIS_RIGHT_Y = 3;
            AXIS_RIGHT_TRIGGER = 4;
            BUTTON_BACK = 8; //left
            BUTTON_START = 9; // power
            // additional buttons: right =  9
            return;
        }
        if (OS.platform.equals(OS.Platform.Windows)) {
            /* Detected as "Razer Serval" by both Windows Bluetooth and libgdx 1.9.4 */
            BUTTON_A = 0;
            BUTTON_X = 2;
            BUTTON_Y = 3;
            BUTTON_B = 1;
            BUTTON_MENU = 11;
            DPAD_IS_POV = true;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = false;
            AXIS_DPAD_X = NOT_USED;
            AXIS_DPAD_Y = NOT_USED;
            BUTTON_L1 /* bumper */= 4;
            BUTTON_L2 /* trigger */= NOT_USED;
            BUTTON_L3 /* joystick */= 8;
            BUTTON_R1 /* bumper */= 5;
            BUTTON_R2 /* trigger */= NOT_USED;
            BUTTON_R3 /* joystick */= 9;
            AXIS_LEFT_X = 3;
            AXIS_LEFT_Y = 2;
            AXIS_LEFT_TRIGGER = NOT_USED;
            AXIS_RIGHT_X = 1;
            AXIS_RIGHT_Y = 0;
            AXIS_RIGHT_TRIGGER = NOT_USED;
            BUTTON_BACK = 6;
            BUTTON_START = 7;
            // additional buttons: left = 12, power =  10
            return;
        }
        if (OS.platform.equals(OS.Platform.Android)) {
            // checked on Razer Forge and NVIDIA Shield
            /* delected as Razer Serval on NVIDIA Shield */
            BUTTON_A = 96;
            BUTTON_X = 99;
            BUTTON_Y = 100;
            BUTTON_B = 97;
            BUTTON_MENU = 82;
            DPAD_IS_POV = false;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = true;
            AXIS_DPAD_X = 8;
            AXIS_DPAD_Y = 9;
            BUTTON_L1 /* bumper */= 102;
            BUTTON_L2 /* trigger */= NOT_USED;
            BUTTON_L3 /* joystick */= 106;
            BUTTON_R1 /* bumper */= 103;
            BUTTON_R2 /* trigger */= NOT_USED;
            BUTTON_R3 /* joystick */= 107;
            AXIS_LEFT_X = 0;
            AXIS_LEFT_Y = 1;
            AXIS_LEFT_TRIGGER = 6; //actually 6 & 7
            AXIS_RIGHT_X = 2;
            AXIS_RIGHT_Y = 3;
            AXIS_RIGHT_TRIGGER = 4; // actually 4 & 5
            BUTTON_BACK = 4;
            BUTTON_START = 110;
            // additional buttons left & right = 0
            return;
        }
    }
}
