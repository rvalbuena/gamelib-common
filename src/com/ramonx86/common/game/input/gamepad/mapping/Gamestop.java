package com.ramonx86.common.game.input.gamepad.mapping;

import com.ramonx86.common.utility.OS;

/**
 * Gamestop Red Samurai branded gamepads; there are several versions
 * but the two I have tested have the same mappings
 * "Tablet controller v2" identifies as "GS Gamepad"
 * "Combination controller & keyboard" identifies as "GS gamepad"
 * Created by ramon on 11/18/15.
 */
public class Gamestop extends GamePad {

    public Gamestop(String modelName){
        super(modelName);
        if (OS.platform.equals(OS.Platform.Windows)) {
            /*
            * libgdx 1.94 detects this as "Bluetooth Wireless Controller"
            * Windows bluetooth config shows "GS gamepad"
            * */
            BUTTON_A = 0;
            BUTTON_X = 2;
            BUTTON_Y = 3;
            BUTTON_B = 1;
            BUTTON_MENU = NOT_USED;
            DPAD_IS_POV = true;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = false;
            AXIS_DPAD_X = NOT_USED;
            AXIS_DPAD_Y = NOT_USED;
            BUTTON_L1 /* bumper */= 4;
            BUTTON_L2 /* trigger */= 6;
            BUTTON_L3 /* joystick */= 10;
            BUTTON_R1 /* bumper */= 5;
            BUTTON_R2 /* trigger */= 7;
            BUTTON_R3 /* joystick */= 11;
            AXIS_LEFT_X = 3;
            AXIS_LEFT_Y = 2;
            AXIS_LEFT_TRIGGER = NOT_USED;
            AXIS_RIGHT_X = 1;
            AXIS_RIGHT_Y = 0;
            AXIS_RIGHT_TRIGGER = NOT_USED;
            BUTTON_BACK = 8;
            BUTTON_START = 9;
            return;
        }
        if (OS.platform.equals(OS.Platform.Linux)) {
            /* detected as "GS gamepad" */
            BUTTON_A = 0;
            BUTTON_X = 2;
            BUTTON_Y = 3;
            BUTTON_B = 1;
            BUTTON_MENU = NOT_USED;
            DPAD_IS_POV = true;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = false;
            AXIS_DPAD_X = NOT_USED;
            AXIS_DPAD_Y = NOT_USED;
            BUTTON_L1 /* bumper */= 4;
            BUTTON_L2 /* trigger */= 6;
            BUTTON_L3 /* joystick */= 10;
            BUTTON_R1 /* bumper */= 5;
            BUTTON_R2 /* trigger */= 7;
            BUTTON_R3 /* joystick */= 11;
            AXIS_LEFT_X = 0;
            AXIS_LEFT_Y = 1;
            AXIS_LEFT_TRIGGER = NOT_USED;
            AXIS_RIGHT_X = 2;
            AXIS_RIGHT_Y = 3;
            AXIS_RIGHT_TRIGGER = NOT_USED;
            BUTTON_BACK = 8;//escape
            BUTTON_START = 9;// enter
            return;
        }
        if (OS.platform.equals(OS.Platform.Android)) {
            /* detected as "GS gamepad" */
            BUTTON_A = 96;
            BUTTON_X = 99;
            BUTTON_Y = 100;
            BUTTON_B = 97;
            BUTTON_MENU = NOT_USED;
            DPAD_IS_POV = false;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = true;
            AXIS_DPAD_X = 4;
            AXIS_DPAD_Y = 5;
            BUTTON_L1 /* bumper */= 102;
            BUTTON_L2 /* trigger */= 104;
            BUTTON_L3 /* joystick */= 106;
            BUTTON_R1 /* bumper */= 103;
            BUTTON_R2 /* trigger */= 105;
            BUTTON_R3 /* joystick */= 107;
            AXIS_LEFT_X = 0;
            AXIS_LEFT_Y = 1;
            AXIS_LEFT_TRIGGER = NOT_USED;
            AXIS_RIGHT_X = 2;
            AXIS_RIGHT_Y = 3;
            AXIS_RIGHT_TRIGGER = NOT_USED;
            BUTTON_BACK = 109;// escape
            BUTTON_START = 108;//enter
            return;
        }
    }
}
