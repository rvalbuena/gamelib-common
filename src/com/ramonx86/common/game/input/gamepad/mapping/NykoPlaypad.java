package com.ramonx86.common.game.input.gamepad.mapping;

import com.ramonx86.common.utility.OS;

/**
 * Nyko Playpad Pro in HID mode
 * NOTE: On Ouya controller example code I think triggers were detected
 * On Windows, named "Bluetooth HID port 0"
 *
 * Created by ramon on 11/21/15.
 */
public class NykoPlaypad extends GamePad {

    public NykoPlaypad (String modelName){
        super(modelName);
        if (OS.platform.equals(OS.Platform.Linux)) {
            /* detected as NYKO PLAYPAD PRO */
            /* button codes are unusually high */
            BUTTON_A = 172;
            BUTTON_X = 175;
            BUTTON_Y = 176;
            BUTTON_B = 173;
            BUTTON_MENU = 17;//not sure about this
            DPAD_IS_POV = true;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = false;
            AXIS_DPAD_X = NOT_USED;
            AXIS_DPAD_Y = NOT_USED;
            BUTTON_L1 /* bumper */= 178;
            BUTTON_L2 /* trigger */= NOT_USED;
            BUTTON_L3 /* joystick */= 185;
            BUTTON_R1 /* bumper */= 179;
            BUTTON_R2 /* trigger */= NOT_USED;
            BUTTON_R3 /* joystick */= 186;
            AXIS_LEFT_X = 0;
            AXIS_LEFT_Y = 1;
            AXIS_LEFT_TRIGGER = 5;
            AXIS_RIGHT_X = 2;
            AXIS_RIGHT_Y = 3;
            AXIS_RIGHT_TRIGGER = 4;
            BUTTON_BACK = 137;
            BUTTON_START = 183;
            return;
        }
        if (OS.platform.equals(OS.Platform.Windows)) {
            /* identified as "Bluetooth Wireless Controller" by Controllers
            * shows as NYKO PLAYPAD PRO in Windows "Manage Bluetooth Settings"
            * triggers are not detected */
            BUTTON_A = 0;
            BUTTON_X = 3;
            BUTTON_Y = 4;
            BUTTON_B = 1;
            BUTTON_MENU = NOT_USED; // read as keystroke, opens default browser
            DPAD_IS_POV = true;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = false;
            AXIS_DPAD_X = 2;
            AXIS_DPAD_Y = 3;
            BUTTON_L1 /* bumper */= 6;
            BUTTON_L2 /* trigger */= NOT_USED;
            BUTTON_L3 /* joystick */= 13;
            BUTTON_R1 /* bumper */= 7;
            BUTTON_R2 /* trigger */= NOT_USED;
            BUTTON_R3 /* joystick */= 14;
            AXIS_LEFT_X = 3;
            AXIS_LEFT_Y = 2;
            AXIS_LEFT_TRIGGER = NOT_USED;//4;
            AXIS_RIGHT_X = 1;
            AXIS_RIGHT_Y = 0;
            AXIS_RIGHT_TRIGGER = NOT_USED;//5;
            BUTTON_BACK = NOT_USED; // read as keystroke, not displayed (special key)
            BUTTON_START = 11;
            return;
        }
        if (OS.platform.equals(OS.Platform.Android)) {
            // checked on Kindle Fire HD6
            // checked on NVIDIA Shield
            /*  detected as NYKO PLAYPAD PRO */
            BUTTON_A = 96;
            BUTTON_X = 99;
            BUTTON_Y = 100;
            BUTTON_B = 97;
            BUTTON_MENU = NOT_USED;// menu
            DPAD_IS_POV = false;
            DPAD_IS_BUTTON = false;
            BUTTON_DPAD_UP = NOT_USED;
            BUTTON_DPAD_DOWN = NOT_USED;
            BUTTON_DPAD_RIGHT = NOT_USED;
            BUTTON_DPAD_LEFT = NOT_USED;
            DPAD_IS_AXIS = true;
            AXIS_DPAD_X = 6;
            AXIS_DPAD_Y = 7;
            BUTTON_L1 /* bumper */= 102;
            BUTTON_L2 /* trigger */= NOT_USED;
            BUTTON_L3 /* joystick */= 106;
            BUTTON_R1 /* bumper */= 103;
            BUTTON_R2 /* trigger */= NOT_USED;
            BUTTON_R3 /* joystick */= 107;
            AXIS_LEFT_X = 0;
            AXIS_LEFT_Y = 1;
            AXIS_LEFT_TRIGGER = 5;
            AXIS_RIGHT_X = 2;
            AXIS_RIGHT_Y = 3;
            AXIS_RIGHT_TRIGGER = 4;
            BUTTON_BACK = 4;
            BUTTON_START = 108;
            return;
        }
    }
}
