package com.ramonx86.common.game.input.gamepad.mapping;

import com.ramonx86.common.utility.OS;

/**
 * Ouya controller
 * Touchpad produces mouse and touch input events
 * Created by ramon on 11/18/15.
 */
public class Ouya extends GamePad {

    public Ouya(String modelName){
        super(modelName);
        if (OS.platform.equals(OS.Platform.Linux)) {
            BUTTON_A = 3;
            BUTTON_X = 4;
            BUTTON_Y = 5;
            BUTTON_B = 6;
            BUTTON_MENU = 17;
            DPAD_IS_POV = false;
            DPAD_IS_BUTTON = true;
            BUTTON_DPAD_UP = 11;
            BUTTON_DPAD_DOWN = 12;
            BUTTON_DPAD_RIGHT = 14;
            BUTTON_DPAD_LEFT = 13;
            DPAD_IS_AXIS = false;
            AXIS_DPAD_X = NOT_USED;
            AXIS_DPAD_Y = NOT_USED;
            BUTTON_L1 /* bumper */= 7;
            BUTTON_L2 /* trigger */= 15;
            BUTTON_L3 /* joystick */= 9;
            BUTTON_R1 /* bumper */= 8;
            BUTTON_R2 /* trigger */= 16;
            BUTTON_R3 /* joystick */= 10;
            AXIS_LEFT_X = 0;
            AXIS_LEFT_Y = 1;
            AXIS_LEFT_TRIGGER = 2;
            AXIS_RIGHT_X = 3;
            AXIS_RIGHT_Y = 4;
            AXIS_RIGHT_TRIGGER = 5;
            BUTTON_BACK = NOT_USED;
            BUTTON_START = NOT_USED;
            return;
        }
        if (OS.platform.equals(OS.Platform.Windows)) {
            /*
            Both libgdx 1.9.4 and Windows bluetooth detected as "OUYA Game Controller"
            NOTE: triggers are very twitchy on Windows, producing random output > 0.6f */
            BUTTON_A = 0;
            BUTTON_X = 1;
            BUTTON_Y = 2;
            BUTTON_B = 3;
            BUTTON_MENU = 14;
            DPAD_IS_POV = false;
            DPAD_IS_BUTTON = true;
            BUTTON_DPAD_UP = 8;
            BUTTON_DPAD_DOWN = 9;
            BUTTON_DPAD_RIGHT = 11;
            BUTTON_DPAD_LEFT = 10;
            DPAD_IS_AXIS = false;
            AXIS_DPAD_X = NOT_USED;
            AXIS_DPAD_Y = NOT_USED;
            BUTTON_L1 /* bumper */= 4;
            BUTTON_L2 /* trigger */= NOT_USED;
            BUTTON_L3 /* joystick */= 6;
            BUTTON_R1 /* bumper */= 5;
            BUTTON_R2 /* trigger */= NOT_USED;
            BUTTON_R3 /* joystick */= 7;
            AXIS_LEFT_X = 1;
            AXIS_LEFT_Y = 0;
            AXIS_LEFT_TRIGGER = 4;
            AXIS_RIGHT_X = 3;
            AXIS_RIGHT_Y = 2;
            AXIS_RIGHT_TRIGGER = 5;
            BUTTON_BACK = NOT_USED;
            BUTTON_START = NOT_USED;
            return;
        }
        if (OS.platform.equals(OS.Platform.Android)) {
            // detected as OUYA Game Controller
            BUTTON_A = 96; // "O"
            BUTTON_X = 97; // "U"
            BUTTON_Y = 98;//"Y"
            BUTTON_B = 99; //"A"
            BUTTON_MENU = 107;
            DPAD_IS_POV = false;
            DPAD_IS_BUTTON = true;
            BUTTON_DPAD_UP = 104;
            BUTTON_DPAD_DOWN = 105;
            BUTTON_DPAD_RIGHT = 108;
            BUTTON_DPAD_LEFT = 109;
            DPAD_IS_AXIS = false;//dpad is also axis?
            AXIS_DPAD_X = NOT_USED;
            AXIS_DPAD_Y = NOT_USED;
            BUTTON_L1 /* bumper */= 100;
            BUTTON_L2 /* trigger */= 110;// also axis!
            BUTTON_L3 /* joystick */= 102;
            BUTTON_R1 /* bumper */= 101;
            BUTTON_R2 /* trigger */= 106;// also axis!
            BUTTON_R3 /* joystick */= 103;
            AXIS_LEFT_X = 0;
            AXIS_LEFT_Y = 1;
            AXIS_LEFT_TRIGGER = 2;// also button!
            AXIS_RIGHT_X = 3;
            AXIS_RIGHT_Y = 4;
            AXIS_RIGHT_TRIGGER = 5;// also button!
            BUTTON_BACK = NOT_USED;
            BUTTON_START = NOT_USED;
            return;
        }
    }

}
