package com.ramonx86.common.utility;


import java.lang.reflect.Field;

/**
 * Utility to determine our current OS, based on code from the following:
 * http://www.badlogicgames.com/forum/viewtopic.php?t=10403&p=47366
 * @author woflieee
 *
 */
public class OS {

    public static enum Platform {
        Windows, Mac, Unix, Solaris, Other, Android, Linux;
    }

    final public static String name;
    final public static Platform platform;
    public static String device;
    public static String manufacturer;
    public static String model;


    static {
        name = System.getProperty("os.name").toLowerCase();
        System.out.println("os.name: " + name);
        do {
            if (System.getProperty("java.vm.name").equalsIgnoreCase("Dalvik")) {
				/*
				* Check for Android devices
				*/
                try {
                    Class<?> buildClass = Class.forName("android.os.Build");
                    Field manufacturerField = buildClass.getDeclaredField("MANUFACTURER");
                    Field modelField = buildClass.getDeclaredField("MODEL");
                    Field deviceField = buildClass.getDeclaredField("DEVICE");

                    Object m = manufacturerField.get(null);
                    if (m != null) {
                        manufacturer = m.toString().toLowerCase();
                        System.out.println("Manufacturer: " + manufacturer);
                    } else { manufacturer = ""; }
                    Object n = modelField.get(null);
                    if (n != null) {
                        model = n.toString().toLowerCase();
                        System.out.println("Model: " + model);
                    } else { model = ""; }
                    Object o = deviceField.get(null);
                    if (o != null) {
                        device = o.toString().toLowerCase();
                        System.out.println("Device: " + device);
                    } else { device = ""; }
                } catch (Exception e) {
                }
                platform = Platform.Android;
                break;
            }
            if (name.contains("linux")) {
                platform = Platform.Linux;
                break;
            }
            if (name.contains("win")) {
                platform = Platform.Windows;
                break;
            }
            if (name.contains("mac")) {
                platform = Platform.Mac;
                break;
            }
            if (name.contains("nix") || name.contains("nux")
                    || name.contains("aix")) {
                platform = Platform.Unix;
                break;
            }
            if (name.contains("sunos")) {
                platform = Platform.Solaris;
                break;
            }
            platform = Platform.Other;
        } while (false);
    }

    public static boolean isOuya(){
        return (platform.equals(Platform.Android) && device.contains("ouya"));
    }

    public static boolean isAndroidDevice(String deviceName) {
        return (platform.equals(Platform.Android) && device.contains(deviceName.toLowerCase()));
    }

    public static boolean isAmazonDevice(){
        return (platform.equals(Platform.Android) && manufacturer.equals("amazon"));
    }

    public static boolean isAmazonDevice(String modelName) {
        // TODO - test on Kindle and Fire TV
        // amazon docs differentiate using model instead of device
        // kindle devices start with "kf", fire tv start with "aft"
        // https://developer.amazon.com/public/solutions/devices/fire-tv/docs/device-and-platform-specifications
        // https://developer.amazon.com/public/solutions/devices/kindle-fire/specifications/01-device-and-feature-specifications
        return isAmazonDevice() && (model.startsWith("kf") || model.startsWith("aft"));
    }

}
