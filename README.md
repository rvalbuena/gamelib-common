# README #

Common library of utility classes for libgdx games

* Version 1.0

* OS class to determine application's current operating system, and on Android devices manufacturer, device name and model info
* GamePad mappings and identifier class to be used with libgdx Controllers 

Planned changes:
* Add convenience functions to check for different Android devices (OUYA, Razer Forge, NVidia Shield, Amazon Fire TV and tablets, etc)
* OS enum - Ouya should be removed, and it should be treated as a "device"

* Possible future revision: convert Java GamePad mapping classes to JSON files, and add new mappings via JSON files instead of additional Java classes